# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.16

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /root/workspace/FillVector

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /root/workspace/FillVector/build

# Include any dependencies generated for this target.
include CMakeFiles/FillVector.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/FillVector.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/FillVector.dir/flags.make

CMakeFiles/FillVector.dir/main.cu.o: CMakeFiles/FillVector.dir/flags.make
CMakeFiles/FillVector.dir/main.cu.o: ../main.cu
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/root/workspace/FillVector/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CUDA object CMakeFiles/FillVector.dir/main.cu.o"
	/usr/local/cuda/bin/nvcc  $(CUDA_DEFINES) $(CUDA_INCLUDES) $(CUDA_FLAGS) -x cu -c /root/workspace/FillVector/main.cu -o CMakeFiles/FillVector.dir/main.cu.o

CMakeFiles/FillVector.dir/main.cu.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CUDA source to CMakeFiles/FillVector.dir/main.cu.i"
	$(CMAKE_COMMAND) -E cmake_unimplemented_variable CMAKE_CUDA_CREATE_PREPROCESSED_SOURCE

CMakeFiles/FillVector.dir/main.cu.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CUDA source to assembly CMakeFiles/FillVector.dir/main.cu.s"
	$(CMAKE_COMMAND) -E cmake_unimplemented_variable CMAKE_CUDA_CREATE_ASSEMBLY_SOURCE

# Object files for target FillVector
FillVector_OBJECTS = \
"CMakeFiles/FillVector.dir/main.cu.o"

# External object files for target FillVector
FillVector_EXTERNAL_OBJECTS =

FillVector: CMakeFiles/FillVector.dir/main.cu.o
FillVector: CMakeFiles/FillVector.dir/build.make
FillVector: CMakeFiles/FillVector.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/root/workspace/FillVector/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CUDA executable FillVector"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/FillVector.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/FillVector.dir/build: FillVector

.PHONY : CMakeFiles/FillVector.dir/build

CMakeFiles/FillVector.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/FillVector.dir/cmake_clean.cmake
.PHONY : CMakeFiles/FillVector.dir/clean

CMakeFiles/FillVector.dir/depend:
	cd /root/workspace/FillVector/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /root/workspace/FillVector /root/workspace/FillVector /root/workspace/FillVector/build /root/workspace/FillVector/build /root/workspace/FillVector/build/CMakeFiles/FillVector.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/FillVector.dir/depend

