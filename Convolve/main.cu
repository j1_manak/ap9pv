#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "utils/pngio.h"

#include "/usr/local/cuda/include/cuda_runtime.h"

#define BLOCK_SIZE (16u)
#define FILTER_SIZE (5u)
#define TILE_SIZE (12u) // BLOCK_SIZE - 2 * (FILTER_SIZE / 2)

#define CUDA_CHECK_RETURN( value ) { \
    cudaError_t err = value; \
    if(err != cudaSuccess) { \
        fprintf(stderr, "Error %s at line %d in file %s", cudaGetErrorString(err), __LINE__, __FILE__); \
        exit( 1 ); \
    } \
}

__device__ unsigned char blurMatrix[FILTER_SIZE][FILTER_SIZE] = {{1,4,6,4,1},{4,16,24,16,4},{6,24,36,24,6},{4,16,24,16,4},{1,4,6,4,1}};
__device__ int focusMatrix[FILTER_SIZE][FILTER_SIZE] = {{0,0,0,0,0},{0,0,-1,0,0},{0,-1,5,-1,0},{0,0,-1,0,0},{0,0,0,0,0}};
__device__ int edgeDetectMatrix[FILTER_SIZE][FILTER_SIZE] = {{0,0,0,0,0},{0,0,1,0,0},{0,1,-4,1,0},{0,0,1,0,0},{0,0,0,0,0}};
__device__ int reliefMatrix[FILTER_SIZE][FILTER_SIZE] = {{0,0,0,0,0},{0,-2,1,0,0},{0,-1,1,1,0},{0,0,1,2,0},{0,0,0,0,0}};


__global__ void processImage(unsigned char *out, unsigned char *in, size_t pitch, unsigned int width, unsigned int height, int func, int dividor) {
    int x_o = TILE_SIZE * blockIdx.x + threadIdx.x;
    int y_o = TILE_SIZE * blockIdx.y + threadIdx.y;

    int x_i = x_o - 2;
    int y_i = y_o - 2;
    int sum = 0;

    __shared__ unsigned char sBuffer[BLOCK_SIZE][BLOCK_SIZE];


    if((x_i >= 0 && x_i < width) && (y_i >= 0 && y_i < height)) {
        sBuffer[threadIdx.y][threadIdx.x] = in[y_i * pitch + x_i];
    } else {
        sBuffer[threadIdx.y][threadIdx.x] = 0;
    }
    __syncthreads(); // synchronizuje vlákna

    if(threadIdx.x < TILE_SIZE && threadIdx.y < TILE_SIZE) {
        for (int r = 0; r < FILTER_SIZE; ++r) {
            for(int c = 0; c < FILTER_SIZE; ++c) {
                switch(func) {
                    case 1:
                        sum += sBuffer[threadIdx.y + r][threadIdx.x + c] * blurMatrix[r][c];
                        break;
                    case 2:
                        sum+= sBuffer[threadIdx.y + r][threadIdx.x + c] * focusMatrix[r][c];
                        break;
                    case 3:
                        sum+= sBuffer[threadIdx.y + r][threadIdx.x + c] * edgeDetectMatrix[r][c];
                        break;
                    case 4:
                        sum+= sBuffer[threadIdx.y + r][threadIdx.x + c] * reliefMatrix[r][c];
                        break;
                }
                
            }
        }
        sum /= dividor;
        // ohraničení přechodu mimo hranice charu
        if(sum > 255) sum = 255;
        else if(sum < 0) sum = 0;

        if(x_o < width && y_o < height) {
        out[y_o * width + x_o] = sum;
        }
    }
}

int getOperation() {
    std::cout << "Vyberte si operaci konvoluce:" << std::endl;
    std::cout << "\t1. Rozostření" << std::endl;
    std::cout << "\t2. Zaostření" << std::endl;
    std::cout << "\t3. Detekce hran" << std::endl;
    std::cout << "\t4. Reliéf" << std::endl;

    std::string val = "";
    int operation = 0;
    bool cinRes = false;

     do {
        std::cin >> val;
        cinRes = sscanf(val.c_str(), "%i", &operation) == 1;
        if(!cinRes) {
            std::cout << "Operace musí být zadána jako číslo." << std::endl;
        }
    } while (!cinRes);

    return operation;
}


int main(){

    int op = getOperation();
    if (op < 1 || op > 4) {
        std::cout << "Vybrána neexistující operace. Konec programu." << std::endl;
        return 0; 
    }

    png::image<png::rgb_pixel> img("../lena.png");

    unsigned int width = img.get_width();
    unsigned int height = img.get_height();

    int size = width * height * sizeof(unsigned char);

    unsigned char *h_r = new unsigned char [size];
    unsigned char *h_g = new unsigned char [size];
    unsigned char *h_b = new unsigned char [size];

    unsigned char *h_r_n = new unsigned char [size];
    unsigned char *h_g_n = new unsigned char [size];
    unsigned char *h_b_n = new unsigned char [size];

    pvg::pngToRgb3(h_r, h_g, h_b, img);

    unsigned char *d_r = NULL;
    unsigned char *d_g = NULL;
    unsigned char *d_b = NULL;

    size_t pitch_r = 0;
    size_t pitch_g = 0;
    size_t pitch_b = 0;

    unsigned char *d_r_n = NULL;
    unsigned char *d_g_n = NULL;
    unsigned char *d_b_n = NULL;

    CUDA_CHECK_RETURN( cudaMallocPitch(&d_r, &pitch_r, width, size) );
    CUDA_CHECK_RETURN( cudaMallocPitch(&d_g, &pitch_g, width, size) );
    CUDA_CHECK_RETURN( cudaMallocPitch(&d_b, &pitch_b, width, size) );

    CUDA_CHECK_RETURN( cudaMalloc(&d_r_n, size) );
    CUDA_CHECK_RETURN( cudaMalloc(&d_g_n, size) );
    CUDA_CHECK_RETURN( cudaMalloc(&d_b_n, size) );

    CUDA_CHECK_RETURN( cudaMemcpy2D(d_r, pitch_r, h_r, width, width, height, cudaMemcpyHostToDevice) );
    CUDA_CHECK_RETURN( cudaMemcpy2D(d_g, pitch_g, h_g, width, width, height, cudaMemcpyHostToDevice) );
    CUDA_CHECK_RETURN( cudaMemcpy2D(d_b, pitch_b, h_b, width, width, height, cudaMemcpyHostToDevice) );

    dim3 gridSize((width + TILE_SIZE - 1) / TILE_SIZE, (height + TILE_SIZE - 1) / TILE_SIZE);
    dim3 blockSize(BLOCK_SIZE, BLOCK_SIZE);

    std::string resFileName = "";
    int dividor = 0;

    switch(op) {
        case 1:
            resFileName = "../lenaBlurred.png";
            dividor = 256;
            break;
        case 2:
            resFileName = "../lenaFocused.png";
            dividor = 1;
            break;
        case 3:
            resFileName = "../lenaEdgeDetect.png";
            dividor = 1;
            break;
        case 4:
            resFileName = "../lenaRelief.png";
            dividor = 1;
            break;
    }
    processImage<<<gridSize, blockSize>>>(d_r_n, d_r, pitch_r, width, height, op, dividor);
    processImage<<<gridSize, blockSize>>>(d_g_n, d_g, pitch_g, width, height, op, dividor);
    processImage<<<gridSize, blockSize>>>(d_b_n, d_b, pitch_b, width, height, op, dividor);

    CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

    CUDA_CHECK_RETURN( cudaMemcpy(h_r_n, d_r_n, size, cudaMemcpyDeviceToHost) );
    CUDA_CHECK_RETURN( cudaMemcpy(h_g_n, d_g_n, size, cudaMemcpyDeviceToHost) );
    CUDA_CHECK_RETURN( cudaMemcpy(h_b_n, d_b_n, size, cudaMemcpyDeviceToHost) );

    pvg::rgb3ToPng(img, h_r_n, h_g_n, h_b_n);
    img.write(resFileName);

    CUDA_CHECK_RETURN( cudaFree(d_r) );
    CUDA_CHECK_RETURN( cudaFree(d_g) );
    CUDA_CHECK_RETURN( cudaFree(d_b) );

    CUDA_CHECK_RETURN( cudaFree(d_r_n) );
    CUDA_CHECK_RETURN( cudaFree(d_g_n) );
    CUDA_CHECK_RETURN( cudaFree(d_b_n) );

    delete [] h_r;
    delete [] h_g;
    delete [] h_b;

    delete [] h_r_n;
    delete [] h_g_n;
    delete [] h_b_n;

    return 0;
}
