#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "/usr/local/cuda/include/cuda_runtime.h"

#define BLOCK_SIZE (4U)

#define CUDA_CHECK_RETURN( value ) { \
    cudaError_t err = value; \
    if(err != cudaSuccess) { \
        fprintf(stderr, "Error %s at line %d in file %s", cudaGetErrorString(err), __LINE__, __FILE__); \
        exit( 1 ); \
    } \
}

__global__ void generateMatrix(unsigned int* arr, int width, int height) {
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;

    if(x < width && y < height) {
        int i = y * width + x;
        arr[i] = i;
    }
}

__global__ void matrixMultiplication(unsigned int* matrixA, unsigned int* matrixB, unsigned int* resMatrix, int matrixAWidth, 
    int matrixAHeight, int matrixBWidth) {

    int blockCol = blockIdx.x;
    int blockRow = blockIdx.y;
    int strideA = matrixAWidth;
    int strideB = matrixBWidth;
    int strideRes = matrixBWidth;
    int sum = 0;

    unsigned int * resMatrixSub = &resMatrix[strideRes * BLOCK_SIZE * blockRow + BLOCK_SIZE * blockCol];

    int row = threadIdx.y;
    int col = threadIdx.x;

    for(int j = 0; j < (matrixAWidth / BLOCK_SIZE); ++j) {
        unsigned int * aSub = &matrixA[strideA * BLOCK_SIZE * blockRow + BLOCK_SIZE * j];
        unsigned int * bSub = &matrixA[strideB * BLOCK_SIZE * j + BLOCK_SIZE * blockRow];

        __shared__ unsigned int aSubShared[BLOCK_SIZE][BLOCK_SIZE];
        __shared__ unsigned int bSubShared[BLOCK_SIZE][BLOCK_SIZE];

        aSubShared[row][col] = aSub[strideA * row + col];
        bSubShared[row][col] = bSub[strideB * row + col];

        __syncthreads();

        for(int k = 0; k < BLOCK_SIZE; ++k) {
            sum += aSubShared[row][k] * bSubShared[k][col];
        }

        __syncthreads();
    }

    resMatrixSub[strideRes * row + col] = sum;
}

void printMatrix(unsigned int* matrix, int width, int heigth, std::string prefix) {
    std::cout << prefix << std::endl;
    for(int i = 0; i < width * heigth; ++i) {
        std::cout << matrix[i] << ", ";
        if(i != 0 && i % width == width - 1) std::cout << std::endl;
    }
}

int main() {
    std::string val;
    bool cinRes = false;
    int matrixAWidth, matrixAHeight, matrixBWidth = 0;
    do {
        std::cout << "Zadejte šířku matice A:" << std::endl;
        std::cin >> val;
        cinRes = sscanf(val.c_str(), "%i", &matrixAWidth) == 1;
        if(!cinRes) {
            std::cout << "Šířka matice musí být zadána jako číslo." << std::endl;
        }
    } while (!cinRes);

    do {
        std::cout << "Zadejte výšku matice A:" << std::endl;
        std::cin >> val;
        cinRes = sscanf(val.c_str(), "%i", &matrixAHeight) == 1;
        if(!cinRes) {
            std::cout << "Šířka matice musí být zadána jako číslo." << std::endl;
        }
    } while (!cinRes);

    do {
        std::cout << "Zadejte šířku matice B:" << std::endl;
        std::cin >> val;
        cinRes = sscanf(val.c_str(), "%i", &matrixBWidth) == 1;
        if(!cinRes) {
            std::cout << "Šířka matice musí být zadána jako číslo." << std::endl;
        }
    } while (!cinRes);

    int matrixASize = sizeof(unsigned int) * matrixAHeight * matrixAWidth;
    int matrixBSize = sizeof(unsigned int) * matrixBWidth * matrixAWidth;

    unsigned int * d_matrixA = NULL;
    unsigned int * h_matrixA = new unsigned int[matrixAWidth * matrixAHeight];
    unsigned int * d_matrixB = NULL;
    unsigned int * h_matrixB = new unsigned int[matrixBWidth * matrixAWidth];

    CUDA_CHECK_RETURN( cudaMalloc(&d_matrixA, matrixASize) );
    CUDA_CHECK_RETURN( cudaMalloc(&d_matrixB, matrixBSize) );

    // udělat grid a block size i pro matici b a výslednou matici
    dim3 maBlockSize(BLOCK_SIZE, BLOCK_SIZE);
    dim3 maGridSize((matrixAWidth + BLOCK_SIZE -1) / BLOCK_SIZE, (matrixAHeight + BLOCK_SIZE - 1) / BLOCK_SIZE);

    dim3 mbBlockSize(BLOCK_SIZE, BLOCK_SIZE);
    dim3 mbGridSize((matrixBWidth + BLOCK_SIZE -1) / BLOCK_SIZE, (matrixAWidth + BLOCK_SIZE - 1) / BLOCK_SIZE);

    generateMatrix<<<maGridSize, maBlockSize>>>(d_matrixA, matrixAWidth, matrixAHeight);
    CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
    generateMatrix<<<mbGridSize, mbBlockSize>>>(d_matrixB, matrixBWidth, matrixAWidth);
    CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

    CUDA_CHECK_RETURN( cudaMemcpy(h_matrixA, d_matrixA, matrixASize, cudaMemcpyDeviceToHost) );
    printMatrix(h_matrixA, matrixAWidth, matrixAHeight, "Matice A:");

    CUDA_CHECK_RETURN( cudaMemcpy(h_matrixB, d_matrixB, matrixBSize, cudaMemcpyDeviceToHost) );
    printMatrix(h_matrixB, matrixBWidth, matrixAWidth, "Matice B");

    dim3 resBlockSize(BLOCK_SIZE, BLOCK_SIZE);
    dim3 resGridSize((matrixBWidth + BLOCK_SIZE -1) / BLOCK_SIZE, (matrixAHeight + BLOCK_SIZE - 1) / BLOCK_SIZE);
    
    int resMatrixSize = sizeof(unsigned int) * matrixBWidth * matrixAHeight;
    unsigned int* d_resMatrix = NULL;
    CUDA_CHECK_RETURN( cudaMalloc(&d_resMatrix, resMatrixSize) );
    unsigned int* h_resMatrix = new unsigned int[matrixBWidth * matrixAHeight];

    matrixMultiplication<<<resGridSize, resBlockSize>>>(d_matrixA, d_matrixB, d_resMatrix, matrixAWidth, matrixAHeight, matrixBWidth);
    CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

    CUDA_CHECK_RETURN( cudaMemcpy(h_resMatrix, d_resMatrix, resMatrixSize, cudaMemcpyDeviceToHost) );
    printMatrix(h_resMatrix, matrixBWidth, matrixAHeight, "A * B = ...");

    CUDA_CHECK_RETURN( cudaFree(d_matrixA) );
    CUDA_CHECK_RETURN( cudaFree(d_matrixB) );
    CUDA_CHECK_RETURN( cudaFree(d_resMatrix) );

    delete [] h_matrixA;
    delete [] h_matrixB;
    delete [] h_resMatrix;

    return 0;
}