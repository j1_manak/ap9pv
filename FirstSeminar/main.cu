#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "/usr/local/cuda/include/cuda_runtime.h"

#define CUDA_CHECK_RETURN( value ) { \
    cudaError_t err = value; \
    if(err != cudaSuccess) { \
        fprintf(stderr, "Error %s at line %d in file %s", cudaGetErrorString(err), __LINE__, __FILE__); \
        exit( 1 ); \
    } \
}

int main() {
    printf("Hello world!\n"); 

    int gpuCount;
    CUDA_CHECK_RETURN(cudaGetDeviceCount(&gpuCount));

    cudaDeviceProp properties;

    for(int i = 0; i < gpuCount; ++i) {
        CUDA_CHECK_RETURN(cudaGetDeviceProperties(&properties, i));
        std::cout << "Device " << i << " name: " << properties.name << std::endl;
        std::cout << "Compute compatibility: " << properties.major << "." <<properties.minor << std::endl;
        std::cout << "Block dimensions: " << properties.maxThreadsDim[0] << ", " << properties.maxThreadsDim[1] <<
            ", " << properties.maxThreadsDim[2] << std::endl;
        std::cout << "Grid dimensions: " << properties.maxGridSize[0] << ", " << properties.maxGridSize[1] <<
            ", " << properties.maxGridSize[2] << std::endl;
    }
    
    printf("Nalezeno %i cuda karet\n", gpuCount);


}