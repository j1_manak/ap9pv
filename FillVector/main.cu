#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "/usr/local/cuda/include/cuda_runtime.h"

#define CUDA_CHECK_RETURN( value ) { \
    cudaError_t err = value; \
    if(err != cudaSuccess) { \
        fprintf(stderr, "Error %s at line %d in file %s", cudaGetErrorString(err), __LINE__, __FILE__); \
        exit( 1 ); \
    } \
}

#define VECT_SIZE (256u)
#define BLOCK_SIZE (128u)

// __device__ -> only gpu
// __host__ -> only cpu
// __global__ -> run on gpu run from cpu

__global__ void fillVector(int *data) {
    int i = threadIdx.x + (blockIdx.x * blockDim.x);
    if (i < VECT_SIZE) data[i] = i + 1;
}

int main() {
    
    // alokace paměti na cpu
    //int *h_data = (int*) malloc(VECT_SIZE * sizeof(int));
    //memset(h_data, 0, VECT_SIZE * sizeof(int));

    // alokace paměti na GPU
    //int *d_data = NULL;
    //CUDA_CHECK_RETURN(cudaMalloc(&d_data, VECT_SIZE * sizeof(int)));

    int *data = NULL;
    CUDA_CHECK_RETURN(cudaMallocManaged(&data, VECT_SIZE * sizeof(int)));

    // konfigurace kernelu
    int blockSize = BLOCK_SIZE;
    int gridSize = ( VECT_SIZE + BLOCK_SIZE - 1 ) / BLOCK_SIZE;

    // spuštění kernelu
    fillVector<<<gridSize, blockSize>>>(data);

    // počkání na dokončení
    CUDA_CHECK_RETURN(cudaDeviceSynchronize());

    // kopírování dat zpět do cpu
    //CUDA_CHECK_RETURN(cudaMemcpy(h_data, d_data, VECT_SIZE * sizeof(int), cudaMemcpyDeviceToHost));

    for(int i = 0; i < VECT_SIZE; ++i) {
        std::cout << data[i];
        if (i < VECT_SIZE - 1) std::cout <<  ", ";
    }
    std::cout << std::endl;

    // uvolnění paměti
    //free(h_data);
    //CUDA_CHECK_RETURN(cudaFree(d_data));
    CUDA_CHECK_RETURN(cudaFree(data));

    return 0;
}