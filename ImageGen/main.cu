#include <stdio.h>
#include <stdlib.h>
#include "utils/pngio.h"
#include <iostream>
#include <cmath>
#include <omp.h>

#include "/usr/local/cuda/include/cuda_runtime.h"

#define WIDTH (800u)
#define HEIGHT (600u)
#define BLOCK_SIZE (16U)

double h_diagLength = 0;

__constant__ double d_diagLength = 0;

#define CUDA_CHECK_RETURN( value ) { \
    cudaError_t err = value; \
    if(err != cudaSuccess) { \
        fprintf(stderr, "Error %s at line %d in file %s", cudaGetErrorString(err), __LINE__, __FILE__); \
        exit( 1 ); \
    } \
}

__global__ void createImg(unsigned char* img) {
    unsigned int x = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned int y = threadIdx.y + blockIdx.y * blockDim.y;

    if((x < WIDTH) && (y < HEIGHT)) {
        unsigned int i = (y * WIDTH + x) * 3;

        img[i] = float(x) / WIDTH * 255;
        img[i + 1] = float(y) / HEIGHT * 255;
        img[i + 2] = sqrtf(powf(x, 2) + powf(y, 2)) / d_diagLength * 255;
    }   
}

int main() {
    std::cout << "Creating image, please wait" << std::endl;
    int size = WIDTH * HEIGHT * 3 * sizeof(unsigned char);
    h_diagLength = sqrtf(powf(WIDTH, 2) + powf(HEIGHT, 2));

    // kopirování konstanty do PGU
    CUDA_CHECK_RETURN( cudaMemcpyToSymbol(d_diagLength, &h_diagLength, sizeof(double)) );

    // alokace paměti na cpu
    unsigned char* h_img = new unsigned char[size];
    // alokace paměti na gpu
    unsigned char* d_img;
    CUDA_CHECK_RETURN( cudaMalloc(&d_img, size) );

    // nastavení parametru kernelu
    dim3 blockSize(BLOCK_SIZE, BLOCK_SIZE);
    dim3 gridSize((WIDTH + BLOCK_SIZE -1) / BLOCK_SIZE, (HEIGHT + BLOCK_SIZE - 1) / BLOCK_SIZE);

    createImg<<<gridSize, blockSize>>>(d_img);
    CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

    // kopírování z gpu
    CUDA_CHECK_RETURN( cudaMemcpy(h_img, d_img, size, cudaMemcpyDeviceToHost) );

    // vytvořit png obrázek
    png::image<png::rgb_pixel> img(WIDTH, HEIGHT);
    pvg::rgbToPng(img, h_img);

    img.write("../test.png");

    CUDA_CHECK_RETURN( cudaFree(d_img) );
    delete[] h_img;

    std::cout << "done" << std::endl;

    return 0;
}