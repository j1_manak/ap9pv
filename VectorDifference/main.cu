#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "/usr/local/cuda/include/cuda_runtime.h"

#define BLOCK_SIZE ( 128u )

#define CUDA_CHECK_RETURN( value ) { \
    cudaError_t err = value; \
    if(err != cudaSuccess) { \
        fprintf(stderr, "Error %s at line %d in file %s", cudaGetErrorString(err), __LINE__, __FILE__); \
        exit( 1 ); \
    } \
}

int* initVector(int size) {
    int *vector = (int*) malloc(size * sizeof(int));
    int value = 0;
    for(int i = 0; i < size; ++i) {
        do {
            value = rand() % 99999;
        } while (value < 1000);

        vector[i] = value;
    }
    return vector;
}

void printArr(int *arr, int size, std::string title) {
    std::cout << title << ": ";
    for (int i = 0; i < size; ++i) {
        if(i == size - 1) std::cout << arr[i];
        else std::cout << arr[i] << ", ";
    }
    std::cout << std::endl;
}

__global__ void doDifference(int *d_vectFirst, int *d_vectSecond, int *d_result, int size) {
    int i = threadIdx.x + (blockIdx.x * blockDim.x);
    if (i < size) d_result[i] = d_vectFirst[i] - d_vectSecond[i];
}

int main() {
    std::cout << "Zadejte velikost vektoru:" << std::endl;
    std::string value = "";
    std::cin >> value;
    int arrLength = 0;
    if(sscanf(value.c_str(), "%i", &arrLength) != 1) {
        std::cout << "Velikost vektoru musí být zadána jako číslo" << std::endl;
        return 0;
    }

    int sizeofArr = arrLength * sizeof(int);

    int *h_vectFirst = initVector(arrLength);
    int *d_vectFirst = NULL;
    CUDA_CHECK_RETURN(cudaMalloc(&d_vectFirst, arrLength * sizeof(int)));
    CUDA_CHECK_RETURN(cudaMemcpy(d_vectFirst, h_vectFirst, sizeofArr, cudaMemcpyHostToDevice));

    int *h_VectSecond = initVector(arrLength);
    int *d_vectSecond = NULL;
    CUDA_CHECK_RETURN(cudaMalloc(&d_vectSecond, arrLength * sizeof(int)));
    CUDA_CHECK_RETURN(cudaMemcpy(d_vectSecond, h_VectSecond, sizeofArr, cudaMemcpyHostToDevice));

    int *d_result = NULL;
    CUDA_CHECK_RETURN(cudaMalloc(&d_result, arrLength * sizeof(int)));

    int blockSize = BLOCK_SIZE;
    int gridSize = ( arrLength + BLOCK_SIZE - 1 ) / BLOCK_SIZE;

    doDifference<<<gridSize, blockSize>>>(d_vectFirst, d_vectSecond, d_result, arrLength);

    CUDA_CHECK_RETURN(cudaDeviceSynchronize()); 

    int *h_result = (int*) malloc(arrLength * sizeof(int));
    CUDA_CHECK_RETURN(cudaMemcpy(h_result, d_result, sizeofArr, cudaMemcpyDeviceToHost));


    printArr(h_vectFirst, arrLength, "Pole 1");
    printArr(h_VectSecond, arrLength, "Pole 2");
    std::cout << "Pole 1 - Pole 2" << std::endl;
    printArr(h_result, arrLength, "Výsledek");

    free(h_vectFirst);
    free(h_VectSecond);
    free(h_result);

    CUDA_CHECK_RETURN(cudaFree(d_vectFirst));
    CUDA_CHECK_RETURN(cudaFree(d_vectSecond));
    CUDA_CHECK_RETURN(cudaFree(d_result));

    return 0;
}